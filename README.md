# points to use git

## To add files into stagging area
* git add . (or) git add file_name

## To check the status of the work
* git status -> it shows the where the work is going on

## To check commits 
* git log -> it shows all commits 

## To show the detail commit
* git show **commit-id**

## To commit the added files
* git commit -m "message"

## To push into gitlab account
* first connect the gitlab remotely using follwing command
    * git remote add origin **link for the gitlab repository**

* and write a command to push as follows
    * git push -u origin master
